<?php

define("INSTALL_URL", "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
define("ROOT_PATH", dirname(dirname(dirname(__FILE__))));
define("INSTALL_PATH", ROOT_PATH . DIRECTORY_SEPARATOR . "public/install");

if (file_exists($vendor = ROOT_PATH . "/vendor/autoload.php")) {
    require_once ($vendor);
}

require_once dirname(__FILE__) . "/install.php";

$install = new Install();

$action = isset($_GET["action"]) ? $_GET["action"] : "index";
if ($action) {
    if (method_exists($install, $action)) {
        return $install->$action();
    }
}

die("Page not found");