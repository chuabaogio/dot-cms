<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dotcms</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/install.css" rel="stylesheet">
    <script src="js/jquery-2.1.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body
<div class="container">
    <div class="row">
        <section>
            <div class="wizard">


                <div class="welcome-area container" <?php if(!$installed){ ?>style="display: none"<?php } ?>>

                    <div class="alert alert-success">
                        Congratulations<br/> <strong>Dotcms</strong> is now installed, Have a nice code :)
                    </div>

                    <div class="alert alert-warning">
                        REMOVE THE INSTALLATION DIRECTORY<br/>
                        You will not be able to proceed beyond this point until the installation directory has been
                        removed. This is a security feature.
                    </div>

                    <br/>
                    <div class="row buttons-area" <?php if($installed){ ?>style="display: none"<?php } ?>>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left text-center">
                            <div class="mycontent-left">
                                <a href="javascript:void(0)" class="btn btn-primary frontend_url" target="_blank">Frontend</a>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right text-center">
                            <div class="mycontent-right">

                                <a href="javascript:void(0)" class="btn btn-primary backend_url"
                                   target="_blank">Backend</a>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="wizard-area container" <?php if($installed){ ?>style="display: none"<?php } ?>>

                    <ul class="nav nav-wizard">
                        <li class="disabled active">
                            <a href="#step1" class="disabled">.server ( )</a>
                        </li>
                        <li class="disabled">
                            <a href="#step2" class="disabled">.config ( )</a>
                        </li>
                        <li class="disabled">
                            <a href="#step3" class="disabled">.user ( )</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="step1">
                            <br/>
                            <?php foreach ($server_errors as $error) { ?>
                                <div class="alert alert-danger">
                                    <?php echo $error; ?>
                                </div>
                            <?php } ?>
                            <?php foreach ($server_messages as $message) { ?>
                                <div class="alert alert-success">
                                    <?php echo $message; ?>
                                </div>
                            <?php } ?>
                            <ul class="list-inline pull-right">
                                <li>
                                    <button type="button"
                                            class="btn btn-primary btn-next <?php if (count($server_errors)) { ?>disabled<?php } ?>">
                                        Next
                                    </button>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="step2">
                            <br/>
                            <div class="bd-example" data-example-id="">
                                <form class="database_form">
                                    <div class="form_errors"></div>


                                    <fieldset>
                                        <legend>Paths :</legend>

                                        <div class="form-group">
                                            <label for="url-input">Base URL :</label>

                                            <input autocomplete="off" type="text" name="app_url" class="form-control"
                                                   id="url-input"
                                                   placeholder="Base URL" value="<?php echo dirname(INSTALL_URL); ?>">
                                        </div>

                                        <div class="form-group">
                                            <label for="admin-prefix-input">Admin url prefix :</label>

                                            <input autocomplete="off" type="text" name="admin_prefix"
                                                   class="form-control"
                                                   id="admin-prefix-input"
                                                   placeholder="Admin url segment"
                                                   value="<?php echo $env["ADMIN_PREFIX"]; ?>">

                                        </div>

                                    </fieldset>

                                    <br/>

                                    <fieldset>
                                        <legend>Database :</legend>

                                        <div class="form-group">
                                            <label for="database-input">Database name :</label>

                                            <input autocomplete="off" type="text" name="db" class="form-control"
                                                   id="database-input"
                                                   placeholder="Database name"
                                                   value="<?php echo $env["DB_DATABASE"] ?>">

                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="createdb" checked="checked"/> Create
                                                    database
                                                </label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="user-input">Host (IP address) :</label>
                                            <input autocomplete="off" type="text" name="host" class="form-control"
                                                   value="<?php echo $env["DB_HOST"] ?>"
                                                   id="host-input" value="localhost"
                                                   placeholder="Host">
                                        </div>

                                        <div class="form-group">
                                            <label for="user-input">User :</label>
                                            <input autocomplete="off" type="text" name="user" class="form-control"
                                                   value="<?php echo $env["DB_USERNAME"] ?>"
                                                   id="user-input"
                                                   placeholder="User">
                                        </div>

                                        <div class="form-group">
                                            <label for="pass-input">Password :</label>
                                            <input autocomplete="off" type="text" name="pass" class="form-control"
                                                   value="<?php echo $env["DB_PASSWORD"] ?>"
                                                   id="pass-input"
                                                   placeholder="password">
                                        </div>

                                    </fieldset>


                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button data-loading-text="Loading..." type="submit"
                                                    class="btn btn-primary">Next
                                            </button>
                                        </li>
                                    </ul>

                                </form>
                            </div>

                        </div>

                        <div class="tab-pane" id="step3">
                            <br/>
                            <div class="bd-example" data-example-id="">
                                <form class="administration_form">
                                    <div class="form_errors"></div>
                                    <fieldset class="form-group">
                                        <label for="title-input">Site title :</label>

                                        <input autocomplete="off" type="text" name="title" class="form-control"
                                               id="title-input"
                                               placeholder="Website title">

                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label for="username-input">Administrator username :</label>
                                        <input autocomplete="off" type="text" name="username" class="form-control"
                                               id="username-input"
                                               placeholder="username">
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label for="userpass-input">Password :</label>
                                        <input autocomplete="off" type="password" name="password" class="form-control"
                                               id="userpass-input" value=""
                                               placeholder="password">
                                    </fieldset>

                                    <fieldset class="form-group">
                                        <label for="reuserpass-input">Confirm password :</label>
                                        <input autocomplete="off" type="password" name="repassword" class="form-control"
                                               id="reuserpass-input" value=""
                                               placeholder="password">
                                    </fieldset>

                                    <ul class="list-inline pull-right">
                                        <li>
                                            <button data-loading-text="Loading..." type="submit"
                                                    class="btn btn-primary">Next
                                            </button>
                                        </li>
                                    </ul>

                                </form>
                            </div>

                        </div>

                        <div class="tab-pane" id="step3">
                            <h3>Step 3</h3>
                            <p>This is step 3</p>
                            <ul class="list-inline pull-right">
                                <li>
                                    <button type="button" class="btn btn-primary btn-next">Continue</button>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="step4">
                            <h3>Complete</h3>
                            <p>You have successfully completed all steps.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>

            </div>
        </section>

        <div class="footer text-center">

        </div>

    </div>
</div>

<script>

    $(document).ready(function () {

        function nextTab() {
            var next_tab = $(".tab-pane.active").next().attr("id");
            $('a').removeAttr('data-toggle');
            $('a[href=#' + next_tab + ']').attr("data-toggle", "tab");
            $('a[href=#' + next_tab + ']').tab('show');
        }

        $(".btn-next").click(function (e) {
            var base = $(this);
            nextTab();
        });

        $(".database_form").submit(function () {

            var base = $(this);
            var error_box = base.find(".form_errors").first();
            var btn = base.find("button[type=submit]");


            var app_url = base.find("input[name=app_url]").first().val();
            var admin_prefix = base.find("input[name=admin_prefix]").first().val();

            btn.button('loading');

            $.post("<?php echo INSTALL_URL; ?>index.php?action=database", base.serialize(), function (result) {

                if (result == "ok") {
                    $(".db_errors").hide();

                    $(".buttons-area").show();

                    $(".frontend_url").attr("href", app_url);
                    $(".backend_url").attr("href", app_url + "/" + admin_prefix);

                    nextTab();

                } else if (result.indexOf('=') > -1) {

                    var html = '<textarea spellcheck="false" name="env" rows="9" style="width:100%">' + result + '</textarea>';

                    error_box.html("<div class='alert alert-danger'>The environment file <strong>.env</strong> is not writable, you can copy/paste this code manually.<br/>" + html + "</div>");

                    $("input[name=createdb]").attr('checked', false);

                } else {
                    error_box.html("<div class='alert alert-danger'>" + result + "</div>");
                }

                btn.button('reset');

            }).fail(function () {
                btn.button('reset');
                error_box.html("server error");
            });

            return false;
        });

        $(".administration_form").submit(function () {

            var base = $(this);
            var error_box = base.find(".form_errors").first();
            var btn = base.find("button[type=submit]");

            btn.button('loading');

            $.post("<?php echo INSTALL_URL; ?>index.php?action=administration", base.serialize(), function (result) {

                if (result == "ok") {
                    $(".db_errors").hide();

                    $(".wizard-area").fadeOut("slow", function () {
                        $(".welcome-area").fadeIn("slow");
                    });


                } else {
                    error_box.html("<div class='alert alert-danger'>" + result + "</div>");
                }

                btn.button('reset');

            }).fail(function () {
                btn.button('reset');
                error_box.html("server error");
            });

            return false;
        });

    });


</script>

<body>
</html>
