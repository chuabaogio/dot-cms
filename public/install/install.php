<?php

use Illuminate\Database\Capsule\Manager as DB;

class Install
{

    public $data = [];
    public $env = [];

    function __construct()
    {
        $this->env = $this->getEnv();
    }

    public function index()
    {

        $server_errors = [];
        $server_messages = [];

        // check php version
        $minimum_php = '5.5.9';

        if (version_compare(PHP_VERSION, $minimum_php, '>=')) {
            $server_messages[] = "<strong>PHP</strong> version: " . PHP_VERSION . ".";
        }else{
            $server_errors[] = "Please update your php to $minimum_php current is " . PHP_VERSION . ".";
        }

        if (file_exists(ROOT_PATH . "/vendor/autoload.php") and file_exists(ROOT_PATH . "/composer.lock")) {
            $server_messages[] = "Composer packages is installed.";
        } else {
            $server_errors[] = "Composer packages is not installed. please run <strong>'composer install'</strong> first.";
        }

        // check mcrypt is installed
        if (!function_exists("mcrypt_encrypt")) {
            $server_errors[] = "<strong>PHP</strong> mcrypt is not installed.";
        } else {
            $server_messages[] = "<strong>PHP</strong> mcrypt is installed.";
        }

        // check storage is writable

        if (!is_writable($storage_path = ROOT_PATH . "/storage")) {
            $server_errors[] = "<strong>Storage path</strong> $storage_path is not writable.";
        } else {
            $server_messages[] = "<strong>Storage path</strong> $storage_path is writable.";
        }


        if (!is_writable($cache_path = ROOT_PATH . "/bootstrap/cache")) {
            $server_errors[] = "<strong>Cache path</strong> $cache_path is not writable";
        } else {
            $server_messages[] = "<strong>Cache path</strong> $cache_path is writable.";
        }

        if (!is_writable($cache_path = ROOT_PATH . "/bootstrap/cache/services.php")) {
            $server_errors[] = "<strong>Cache path</strong> $cache_path is not writable";
        } else {
            $server_messages[] = "<strong>Cache path</strong> $cache_path is writable.";
        }

        if (!is_writable($uploads_path = ROOT_PATH . "/public/uploads")) {
            $server_errors[] = "<strong>Uploads path</strong> $uploads_path is not writable.";
        } else {
            $server_messages[] = "<strong>Uploads path</strong> $uploads_path is writable.";
        }

        // check Sitemaps path is writable
        if (!is_writable($sitemaps_path = ROOT_PATH . "/public/sitemaps")) {
            $server_errors[] = "<strong>Sitemaps path</strong> $sitemaps_path is not writable.";
        } else {
            $server_messages[] = "<strong>Sitemaps path</strong> $sitemaps_path is writable.";
        }


        $this->data["server_errors"] = $server_errors;
        $this->data["server_messages"] = $server_messages;
        $this->data["env"] = $this->env;


        $this->data["installed"] = file_exists(ROOT_PATH . "/storage/dot");

        $this->render("form", $this->data);

    }

    function database()
    {

        $app_url = $this->post("app_url");
        $admin_prefix = $this->post("admin_prefix");


        if ($app_url == "") {
            die("Missing application base url.");
        }

        if (!strstr($app_url, "http")) {
            die("Invalid application base url.");
        }

        if ($admin_prefix == "") {
            die("Missing admin url prefix.");
        }

        if (strstr($admin_prefix, " ")) {
            die("Invalid admin url prefix.");
        }

        $db = $this->post("db");
        $host = $this->post("host");
        $user = $this->post("user");
        $pass = $this->post("pass");

        if ($host == "localhost") {
            $host = "127.0.0.1";
        }

        if ($db == "" or $user == "" or $host == "") {
            die("Invalid database server connection settings");
        }

        $connection = NULL;
        $error = NULL;

        try {
            $connection = @new mysqli($host, $user, $pass);
        } catch (Exception $error) {
            // :(
        }

        if ($connection->connect_errno) {
            die($connection->connect_error);
        }

        if ($this->post("createdb")) {
            $sql = "CREATE DATABASE " . $db;
            if ($connection->query($sql) === false) {
                die("Error creating database: " . $connection->error);
            }
        }

        if ($connection->select_db($db) === false) {
            die("Database '$db' is not exist.");
        }

        // Check if .env file is pasted
        if ($this->env["APP_URL"] != $app_url or $this->env["ADMIN_PREFIX"] != $admin_prefix or $this->env["DB_DATABASE"] != $db or $this->env["DB_HOST"] != $host or $this->env["DB_USERNAME"] != $user or $this->env["DB_PASSWORD"] != $pass) {

            // saving .env file

            $this->env["APP_URL"] = $app_url;
            $this->env["ADMIN_PREFIX"] = $admin_prefix;

            $this->env["DB_DATABASE"] = $db;
            $this->env["DB_HOST"] = $host;
            $this->env["DB_USERNAME"] = $user;
            $this->env["DB_PASSWORD"] = $pass;

            $this->saveEnv();
        }

        // Closing connection
        $connection->close();

        @chmod(ROOT_PATH, 0777);

        // Run migrations files
        exec("php " . ROOT_PATH . '/artisan dot:migrate --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate options --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate users --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate roles --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate pages --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate media --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate galleries --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate tags --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate categories --env=development --quiet');
        exec("php " . ROOT_PATH . '/artisan module:migrate posts --env=development --quiet');

        // Publishing assets and config files
        exec("php " . ROOT_PATH . '/artisan vendor:publish --force --env=development --quiet');


        die("ok");

    }

    function administration()
    {

        $this->createConnection();

        $title = $this->post("title");
        $username = $this->post("username");
        $password = $this->post("password");
        $repassword = $this->post("repassword");


        if ($title == "") {
            die("Missing title.");
        }

        if ($username == "") {
            die("Missing username.");
        }

        if (mb_strlen($username) < 3) {
            die("Username must be at least 3 characters long.");
        }

        if ($password == "") {
            die("Missing password.");
        }

        if (mb_strlen($password) < 3) {
            die("Password must be at least 3 characters long.");
        }

        if ($username == "") {
            die("username must be at least 3 characters long");
        }

        if ($password != $repassword) {
            die("password doesn't match confirm password field.");
        }

        DB::table("options")->where("name", "site_title")->update(["value" => $title]);
        DB::table("options")->where("name", "site_name")->update(["value" => $title]);

        $hashed_password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);

        DB::table("users")->where("root", 1)->update([
            "username" => $username,
            "password" => $hashed_password,
            "first_name" => "admin"
        ]);


        @file_put_contents(ROOT_PATH . "/storage/dot", "");

        die("ok");

    }

    function post($key, $default = false)
    {
        return isset($_POST[$key]) ? $_POST[$key] : $default;
    }

    private function render($view, $data = [])
    {
        extract($data);
        require_once dirname(__FILE__) . "/" . $view . ".php";
    }

    function getEnv()
    {
        $vars = [];

        if (file_exists(ROOT_PATH . "/.env")) {
            foreach (file(ROOT_PATH . "/.env") as $line) {
                if (strpos(trim($line), '#') === 0 || strpos($line, '=') === false) {
                    continue;
                }
                list($name, $value) = array_map('trim', explode('=', $line, 2));
                $vars[$name] = $value;
            }
        }

        return $vars;
    }


    function saveEnv()
    {

        ksort($this->env);

        $content = '';

        foreach ($this->env as $key => $val) {
            $content .= "$key=$val\n";
        }

        @chmod(ROOT_PATH . "/.env", 0777);

        if (!is_writable(ROOT_PATH . "/.env")) {
            die($content);
        } else {
            file_put_contents(ROOT_PATH . "/.env", $content);
        }

    }

    function createConnection()
    {

        $db = new DB;
        $db->addConnection([
            'driver' => 'mysql',
            'host' => $this->env["DB_HOST"],
            'database' => $this->env["DB_DATABASE"],
            'username' => $this->env["DB_USERNAME"],
            'password' => $this->env["DB_PASSWORD"],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);

        $db->setAsGlobal();
        $db->bootEloquent();

    }

}
