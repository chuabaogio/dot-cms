<?php

define('LARAVEL_START', microtime(true));


/*
|--------------------------------------------------------------------------
| Check if the installation directory is still exists
|--------------------------------------------------------------------------
|
| Redirect if install directory exists
|
*/

if (basename($_SERVER["SCRIPT_FILENAME"]) == "index.php") {

    if (file_exists(__DIR__ . '/../public/install')) {

        $url = "";

        if (isset($_SERVER['HTTPS'])) {
            $url .= "https://";
        } else {
            $url .= "http://";
        }

        $url .= $_SERVER['HTTP_HOST'];

        if ($_SERVER["SERVER_PORT"] != 80) {
            $url .= ":" . $_SERVER["SERVER_PORT"];
        }

        $url .= rtrim(dirname($_SERVER['SCRIPT_NAME']), "/") . "/install";

        return header("location: $url");
    }
}

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__ . '/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

$compiledPath = __DIR__ . '/cache/compiled.php';

if (file_exists($compiledPath)) {
    require $compiledPath;
}
